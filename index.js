const express = require('express');
const getData = require('./libs/getAllInfo.js');
require('dotenv').config()
const app = express();
const port = process.env.PORT;

function convertBytes(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    if (bytes === 0) return '0 Byte';
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    const converted = (bytes / Math.pow(1024, i)).toFixed(2);
    return `${converted} ${sizes[i]}`;
}

const formatDate = (timestamp) => {
    const date = new Date(timestamp * 1000); // Convertir a milisegundos
    return date.toISOString(); // Formato ISO
};

app.get('/terabox', async (req, res) => {
    const shortUrl = req.query.url;
    const params = {}; 

    try {
        const data = await getData(shortUrl, params);

        if (!data.success) {
            return res.status(400).json({ success: false, message: 'Failed to get data', data });
        }

        const formattedData = {
            name: data.data.download.server_filename,
            shareUsername: data.data.info.share_username,
            imageUserShare: data.data.info.head_url,
            size: convertBytes(parseInt(data.data.download.size)),
            thumbnails: data.data.download.thumbs || null,
            directLink: data.data.download.dlink,
            category: data.data.download.category,
            createTime: formatDate(data.data.download.server_ctime),
            updateTime: formatDate(data.data.download.server_mtime),
            country: data.data.info.country,
            shareId: data.data.info.shareid,
            uk: data.data.info.uk
        };

        res.json({ success: true, data: formattedData });
    } catch (error) {
        res.status(500).json({ success: false, message: error.message });
    }
});

app.listen(port, () => {
    console.log(`API running on http://localhost:${port}`);
});

