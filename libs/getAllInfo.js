const getAllInfo = require('./getInfo');
const getDownloadInfo = require('./getDownload');

function processTeraBoxUrl(url) {
    let url1, url2;

    // Función para obtener el valor después de un parámetro en la URL
    function getParamValue(paramName) {
        const regex = new RegExp(`${paramName}=([^&]+)`);
        const match = url.match(regex);
        return match ? match[1] : null;
    }

    // Verificar si la URL tiene el parámetro surl=
    const surlValue = getParamValue('surl');
    if (surlValue) {
        // Asignar la URL sin modificar y con '1' al principio
        url1 = surlValue;
        url2 = '1' + surlValue;
    } else {
        // Obtener el valor final de la URL después del último '/'
        const lastIndex = url.lastIndexOf('/');
        url2 = url.substring(lastIndex + 1);

        // Crear la URL con '1' al principio
        url1 = url2.slice(1);
    }

    return { url1, url2 };
}

async function getData(url) {
    
    try {
        const { url1, url2 } = await processTeraBoxUrl(url);
        console.log(url1, url2)
        const params = {
            jsToken: '974193CF1F70F8696DD88544FBD93EFF7C3BC66FB5FC1ED227EF2CE2456B146EBCC417A52CB714EB66B6D71463B14F359280F29317941080E4E0482D0B3BE5A3',
            dpLogId: '66624800429274820018',
        };

        // Fetch data from both getInfo and getDownloadInfo concurrently
        const [infoResponse, downloadResponse] = await Promise.all([
            getAllInfo(url2),
            getDownloadInfo(url1, params)
        ]);

        // Check if both requests were successful
        const success = infoResponse.ok && downloadResponse.ok;

        if (success) {
            return {
                success: true,
                data: {
                    info: infoResponse.data,
                    download: downloadResponse.data
                }
            };
        } else {
            return {
                success: false,
                error: infoResponse.success ? downloadResponse.message : infoResponse.message || downloadResponse.message
            };
        }

    } catch (error) {
        return {
            success: false,
            message: error.message
        };
    }
}

module.exports = getData;
