const puppeteer = require('puppeteer');

const getAllInfo = async (shortUrl) => {
    let browser;
    try {
        browser = await puppeteer.launch();
        const page = await browser.newPage();

        // Construimos la URL con los parámetros necesarios
        const url = `https://www.terabox.com/api/shorturlinfo?app_id=250528&shorturl=${shortUrl}&root=1`;

        // Navegamos a la URL
        await page.goto(url, { waitUntil: 'networkidle0' });

        // Obtenemos el contenido de la respuesta como JSON
        const response = await page.evaluate(() => {
            return JSON.parse(document.querySelector("body").innerText);
        });

        // Cerramos el navegador
        await browser.close();

        // Verificamos si hubo un error en la respuesta
        if (response.errno !== 0) throw new Error("Failed to get data");

        const list = response.list.map((file) => ({
            category: file.category,
            fs_id: file.fs_id,
            is_dir: file.isdir,
            size: file.size,
            filename: file.server_filename,
            create_time: file.server_ctime,
        }));

        return {
            ok: true,
            data: {
                shareid: response.shareid,
                uk: response.uk,
                sign: response.sign,
                timestamp: response.timestamp,
                list: list,
                allow: response
            }
        };
    } catch (error) {
        if (browser) {
            await browser.close();
        }
        return {
            ok: false,
            message: error.message
        };
    }
}

module.exports = getAllInfo;
