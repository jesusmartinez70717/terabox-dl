const puppeteer = require('puppeteer');

const getDownloadInfo = async (shortUrl, params) => {
    let browser;
    try {
        browser = await puppeteer.launch({ headless: true  });
        const page = await browser.newPage();

        // Agregar encabezados adicionales para simular un navegador real
        await page.setExtraHTTPHeaders({
            'Cookie': 'browserid=q6UfZzRg0htykvpT5P2J0hvqZYLzd14iaRLa3-aFr42RGp4aclY4etLQb4IhOwkspraRMmHWydCYiIxu; lang=es; TSID=HFZ0CtXgl918EEGr4VIgM5pqaQBqfcSs; PANWEB=1; __bid_n=190234edf8366174ab4207; csrfToken=JtxcHeUB0Z6THdBRaJhrNHVB; __stripe_mid=8d47fa10-54e0-468c-8560-c8618b0929afbec746; g_state={"i_l":0}; ndus=Y499ZC4teHuit5P9PDuij6i3WMHQjM8PsZOYIuHh; ndut_fmt=133A9990B71BAD542BE63945F76DFC3E60C05087B8108D243475B66443C1FFCC; ab_sr=1.0.1_ZjBmODhjYTAxMTY3NTY1NGMzMDE5Nzg0ZDliMjNhYzViNzg2ZTUwOTljZjhhZTllYWU2ZDFjNDJhODVhYWNlMTA3ZmQ0ZjYxNjFmZWM2YTNkODllMWIzMGY2MzY3NTNhMGY1NGNjYWE1YTRiOGEyNGMyMGEyM2E2ZjcyMTA3MWMyMjM5YjAxYjdmMzZkYmJkMDhkYTAwN2JmNzcxMjI4ZQ==',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36',
            "Accept-Language": "en-US,en;q=0.5",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
        });

        const queryString = new URLSearchParams({
            app_id: params.app_id || '250528',
            web: params.web || '1',
            channel: params.channel || 'dubox',
            clienttype: params.clienttype || '0',
            jsToken: params.jsToken,
            'dp-logid': params.dpLogId,
            page: params.page || '1',
            num: params.num || '20',
            by: params.by || 'name',
            order: params.order || 'asc',
            site_referer: params.siteReferer || '',
            shorturl: shortUrl,
            root: params.root || '1'
        }).toString();

        const url = `https://www.terabox.com/share/list?${queryString}`;
        await page.goto(url, { waitUntil: 'networkidle0' });

        // Verificar el contenido de la respuesta y manejar casos en los que no se encuentra "list[0]"
        const response = await page.evaluate(() => {
            const bodyText = document.querySelector("body").innerText;
            let data;
            try {
                data = JSON.parse(bodyText);
            } catch (e) {
                data = { error: "Failed to parse response" };
            }
            return data;
        });

        await browser.close();

        if (response.errno !== 0) {
            throw new Error(`API Error: ${response.errno}`);
        }

        const data = response.list && response.list.length > 0 ? response.list[0] : response;

        return { ok: true, data: data };
    } catch (error) {
        if (browser) {
            await browser.close();
        }
        return {
            ok: false,
            message: error.message,
        };
    }
};

module.exports = getDownloadInfo;
